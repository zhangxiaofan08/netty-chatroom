package org.zhangxf.client;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @AUTHOR zhangxf
 * @CREATE 2020-03-17 01:04
 */
public class NettyChatClientHandler extends SimpleChannelInboundHandler<String> {

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, String s) throws Exception {
        System.out.println("channelRead0 ： " + s.trim());
    }
}
